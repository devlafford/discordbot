from Character import Character
from Character import PlayerCharacter

class Player:
    def __init__(self, name):
        self.name = name
        self.player_character = PlayerCharacter("dingus")
        self.family = []
        self.inventory = []
        self.money = 0

        # timestamps
        self.ts_daily = 0
        self.ts_rob   = 0
