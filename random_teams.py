#!/usr/bin/env python3
# read the players list

import json

players = []

def ratio(a, b):
    if b:
        return a/b
    else:
        return a

class Player:
    def __init__(self, name):
        self.name    = name
        self.wins    = 0
        self.losses  = 0
        self.kills   = 0
        self.deaths  = 0
        self.assists = 0

    def json_serialize(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

def read_players(players_filename):
    player_names = []
    with open(players_filename) as players_fp:
        for line in players_fp:
            player = line.strip()
            if len(player) > 0:
                player_names.append(player)
    return player_names

def print_scoreboard(players):
    players.sort(key=lambda player : ratio(player.wins,(player.wins + player.losses)*100))
    print("|       PLAYER       |  K  |  D  |  A  |  W  /  L  (%)     |")
    for player in players:
        print("|{:^20}|{:^5}|{:^5}|{:^5}|{:^5}/{:^5}({:2.2f}%) |".format(
        player.name,
        player.kills,
        player.deaths,
        player.assists,
        player.wins,
        player.losses,
        ratio(player.wins,(player.wins + player.losses)*100)))

### COMMANDS ###

def new_game(): # reset
    player_names = read_players("players.txt")
    for name in player_names:
        players.append(Player(name))
        print("Created {}".format(name))
    return players

def save_state(): # save
    with open("tourney_state.json", "w") as state_fp:
        state_fp.write(json.dumps(players))

def load_state(): # load
    with open("tourney_state.json", "r") as state_fp:
        return json.loads(state_fp)

def print_scoreboard(): # scoreboard
    players.sort(key=lambda player : ratio(player.wins,(player.wins + player.losses)*100))
    print("|       PLAYER       |  K  |  D  |  A  |  W  /  L  (%)     |")
    for player in players:
        print("|{:^20}|{:^5}|{:^5}|{:^5}|{:^5}/{:^5}({:2.2f}%) |".format(
        player.name,
        player.kills,
        player.deaths,
        player.assists,
        player.wins,
        player.losses,
        ratio(player.wins,(player.wins + player.losses)*100)))

def print_help(commands): # bad command
    print("Available commands are:")
    for command in commands.keys():
        print(" -{}".format(command))
    

def setup_commands():
    commands = {}
    commands['reset']      = new_game
    commands['save']       = save_state
    commands['load']       = load_state
    commands['scoreboard'] = print_scoreboard
    commands['quit']       = exit
    return commands

if __name__ == "__main__":
    commands = setup_commands()
    while True:
        command = input("Input command:")
        if command in commands:
            commands[command]()
        else:
            print_help(commands)



    
    




