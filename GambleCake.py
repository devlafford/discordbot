#!/usr/bin/python3
import asyncio
import time
import random
import datetime
from threading import Thread
import discord

from Player import Player
from Character import Character
from Character import NonPlayerCharacter
#import r6sapi as api

# Load the token from a file, do not put this token on the internet
with open('token.do_not_commit') as token_file:
    TOKEN = token_file.readline()

TRIGGER = 'gc'
NAME = 'GambleCake' 
client = discord.Client()
#auth = api.Auth("email","password")     ## used for accessing player info, well probalby need dummy account
bots = {}

################
### COMMANDS ###
################
async def cmd_print_usage(bot, args, message):
    outgoing = "{} USAGE: \n".format(NAME)
    outgoing += "There are {} commands:\n".format(len(commands))
    
    for k,v in commands.items():
        outgoing += '{}: \t{}\n'.format(k, v[1])

    await message.channel.send(outgoing)

async def cmd_insult(bot, args, message):
    await message.channel.send("Go fuck yourself.")

async def cmd_family(bot, args, message):
    player = bot.players[message.author.name]
    outgoing = "@{}'s family members: ".format(player.name)
    for member in player.family:
        outgoing += "{} ({}), ".format(member.name, member.relation_type)
    await message.channel.send(outgoing)

async def cmd_status(bot, args, message):
    player = bot.players[message.author.name]
    status = player.player_character.status
    
    outgoing = "{}'s status:\n".format(player.name)
    for k,v in status.items():
        outgoing += "{}: {}\n".format(k, v)
    
    await message.channel.send(outgoing)

async def cmd_stats(bot, args, message):
    player = bot.players[message.author.name]
    stats = player.player_character.stats
    
    outgoing = "{}'s stats:\n".format(player.name)
    for k,v in stats.items():
        outgoing += "{}: {}\n".format(k, v)
    
    await message.channel.send(outgoing)

async def cmd_leaderboard(bot, args, message):
    outgoing = "### LEADERBOARD ###\n"

    # sort player list by money
    ordered_leaderboard = sorted(bot.players.items(), key=lambda item: item[1].money)

    for item in ordered_leaderboard:
        outgoing += "{}: {}\n".format(item[0], item[1].money)

    await message.channel.send(outgoing)

async def cmd_daily(bot, args, message):
    player = bot.players[message.author.name]
    if time.time() > player.ts_daily + 86400:
        player.money += 500
        player.ts_daily = time.time()
        await message.channel.send("@{} Successfully claimed their daily handout!".format(player.name))
    else:
        delta = str(datetime.timedelta(seconds=round(player.ts_daily+86400-time.time())))
        await message.channel.send("@{} You need to wait {} for another handout. Lazy bastard.".format(player.name, delta))

async def cmd_rob(bot, args, message):
    if len(args) != 1:
        await message.channel.send("Usage: {} <player name>".format(TRIGGER))
        await message.channel.send("If you want this feature to just randomly rob someone tell Devin to write that".format(TRIGGER))
        return
    target = args[0]

    # check that you are allowed to rob
    player = bot.players[message.author.name]
    if time.time() < player.ts_rob + 3600:
        delta = str(datetime.timedelta(seconds=round(player.ts_rob+3600-time.time())))
        await message.channel.send("@{} You need to wait {} to rob someone else.".format(player.name, delta))
        return

    # check that robbery target exists and is not you
    if target not in bot.players.keys() or target == player.name:
        await message.channel.send("{} is not a valid robbery target.".format(target))
        return
    target = bot.players[target]

    # check that robbery target has enough money to be robbed
    if target.money < 250:
        await message.channel.send("{} does not have 250 on them, not worth robbing.".format(target.name))
        return

    # 20% chance to rob
    randval = random.random()
    if randval > 0.20: # fail robbery
        player.ts_rob = time.time()
        player.money -= 250
        if player.player_character.status['reputation'] > 1:
            player.player_character.status['reputation'] -= 1
        await message.channel.send("{} got caught stealing! ({}) Fined ${}".format(player.name, randval, 250))
        return

    # randomize how much is stolen?
    randval = random.random() / 2.0 # can take at most half their money
    transfer_amt = int(randval * 100 * target.money)
    target.money -= transfer_amt
    player.money += transfer_amt
    await message.channel.send("{} stole ${} from {}".format(player.name, transfer_amt, target.name))

# TODO: put the slots data somewhere else
# $100 slot machine
# ❌ - win nothing
# 🍉 - watermelon (win $200)
# 🍒 - cherry (win $250)
# 🔔 - bell (win $500)
# 💎 - diamond (win $1000)
# 💯 - jackpot (win current jackpot)

# funny emoji to use for something else
# 🍆
# 🍑
# 💦
# 👀

slot_tokens = {}
slot_tokens['❌'] = 0
slot_tokens['🍉'] = 200
slot_tokens['🍒'] = 500
slot_tokens['🔔'] = 1000
slot_tokens['💎'] = 2500
slot_tokens['💯'] = 5000

slots0 = ['❌', '🍉', '🍒', '🍉', '🔔', '🍉', '💎', '❌', '💯', '❌', '🍒', '🍉', '🔔', '🍒', '🔔', '🍉', '💎', '🍒', '💯', '🍉', '❌']
slots1 = ['❌', '🍉', '🍒', '🍉', '🔔', '🍉', '💎', '❌', '💯', '❌', '🍒', '🍉', '🔔', '🍒', '🔔', '🍉', '💎', '🍒', '💯', '🍉', '❌']
slots2 = ['❌', '🍉', '🍒', '🍉', '🔔', '🍉', '💎', '❌', '💯', '❌', '🍒', '🍉', '🔔', '🍒', '🔔', '🍉', '💎', '🍒', '💯', '🍉', '❌']
async def cmd_slots(bot, args, message): #TODO: send all in one message
    player = bot.players[message.author.name]

    # check if they have enough money
    if (player.money < 100):
        await message.channel.send("You don't have enough money to play the slots. You need $100.")
        return
    player.money -= 100

    # roll the slot
    slot0 = random.randrange(1,20)
    slot1 = random.randrange(1,20)
    slot2 = random.randrange(1,20)
    await message.channel.send("SLOT MACHINE:\n{} {} {}\n{} {} {}\n{} {} {}".format(
    slots0[slot0-1], slots1[slot1-1], slots2[slot2-1],
    slots0[slot0-0], slots1[slot1-0], slots2[slot2-0],
    slots0[slot0+1], slots1[slot1+1], slots2[slot2+1]
    ))

    # check wins
    win_row0  = slots0[slot0-1] == slots1[slot1-1] == slots2[slot2-1]
    win_row1  = slots0[slot0+0] == slots1[slot1+0] == slots2[slot2+0]
    win_row2  = slots0[slot0+1] == slots1[slot1+1] == slots2[slot2+1]
    win_diag0 = slots0[slot0-1] == slots1[slot1+0] == slots2[slot2+1]
    win_diag1 = slots0[slot0+1] == slots1[slot1+0] == slots2[slot2-1]

    if not (win_row0 or win_row1 or win_row2 or win_diag0 or win_diag1): # didn't win, increase jackpot
        bot.jackpot_amt += 25
        await message.channel.send("You didn't win. Jackpot is {}".format(bot.jackpot_amt))
        return

    # determine win amount
    winning_token = None
    if win_row0:
        winning_token = slots0[slot0-1]
    elif win_row1:
        winning_token = slots0[slot0+0]
    elif win_row2:
        winning_token = slots0[slot0+1]
    elif win_diag0:
        winning_token = slots0[slot0-1]
    elif win_diag1:
        winning_token = slots0[slot0+1]

    # this is bad code but having the emojis in it makes it cute
    if winning_token == '❌':
        win_amt = 0
    elif winning_token == '🍉':
        win_amt = 200
    elif winning_token == '🍒':
        win_amt = 500
    elif winning_token == '🔔':
        win_amt = 1000
    elif winning_token == '💎':
        win_amt = 2500
    elif winning_token == '💯':
        win_amt = jackpot_amt
        jackpot_amt = 5000

    player.money += win_amt
    await message.channel.send("You win ${}! Jackpot is {}".format(win_amt, bot.jackpot_amt))

#######################
### HIDDEN COMMANDS ###
#######################
async def hcmd_test(bot, args, message):
    await message.channel.send("this command just repeats args back to you: {}".format(args))

async def hcmd_diagnostic(bot, args, message):
    outgoing = "DIAGNOSTIC COMMAND\n"
    for name, player in bot.players.items():
        outgoing += "{}".format(name)
        
    await message.channel.send("This command has not been implemented yet.")

async def hcmd_setmoney(bot, args, message):
    # check args
    if len(args) != 2:
        await message.channel.send("Usage: {} setmoney <player name> <$ amount>".format(TRIGGER))
        return
        
    playername = args[0]
    money = args[1]
    if playername not in bot.players.keys():
        await message.channel.send("That player does not exist.")
        return    

    try:
        money = int(money)
    except:
        await message.channel.send("Invalid $ amount")
        return
        
    bot.players[playername].money = money
    await message.channel.send("Set {}'s money to {}".format(playername, money))

async def hcmd_addfamily(bot, args, message):
    player = bot.players[message.author.name]
    new_member = NonPlayerCharacter("member_{}".format(len(player.family))) # name them member_0 etc
    new_member.relation_type = "debug member"
    new_member.relation_target = player.name
    player.family.append(new_member)
    
####################
### COMMAND LIST ###
####################
# TODO: sectionize these
commands = {}
# meta
commands['help']                = (cmd_print_usage, "Print this message." )
commands['kys']                 = (cmd_insult,      "You're a dipshit.")
# player character
commands['family']              = (cmd_family,      "Show members of your family.")
commands['stats']               = (cmd_stats,       "Display your player character's stats.")
commands['status']              = (cmd_status,      "Display your player character's status.")
commands['leaderboard']         = (cmd_leaderboard, "Show the leaderboard.")
# money
commands['daily']               = (cmd_daily,       "Claim your daily government-funded handouts.")
commands['rob']                 = (cmd_rob,         "Rob someone. Works better with a weapon.")
# gambling
commands['slots']               = (cmd_slots,       "Play the slots machine.")

#                  devlafford
hidden_user_ids = [256595200540016640]
hidden_commands = {}
hidden_commands['test']         = (hcmd_test,       "Repeats args back to you" )
hidden_commands['diagnostic']   = (hcmd_diagnostic, "diagnostics")
hidden_commands['setmoney']     = (hcmd_setmoney,   "set money")
hidden_commands['addfamily']    = (hcmd_addfamily,  "create a fake family member")

###################################
### BOT INSTANCE FOR EACH GUILD ###
###################################
class GambleCake:

    ### INIT ###
    def __init__(self, guild):

        self.guild = guild
        self.botlog_channel = discord.utils.get(self.guild.channels, name='bot-log')
        self.players = {}
        self.jackpot_amt = 5000

    async def online_message(self):
        logon_message = "GambleCake coming online to {} at {}".format(self.guild.name, datetime.datetime.now().strftime("%H:%M:%S"))
        await self.botlog_channel.send(logon_message)

##########################
### DISCORD API EVENTS ###
##########################
@client.event
async def on_message(message):
    channel = message.channel
    # we do not want the bot to reply to itself
    if message.author == client.user:
        return

    if message.content.startswith(TRIGGER):
        original = message.content
        incoming = message.content.replace(TRIGGER, '').lstrip()
        command = incoming.split()[0]
        bot = bots[message.guild.id]
        args = incoming.split()[1:]

        # check that the player exists, create them if they do not
        if message.author.name not in bot.players:
            bot.players[message.author.name] = Player(message.author.name)

        # public commands
        if incoming.split()[0] in commands:
            await commands[incoming.split()[0]][0](bot, args, message)

        # non public commands
        elif incoming.split()[0] in hidden_commands and message.author.id in hidden_user_ids: # non-public commands
            await hidden_commands[incoming.split()[0]][0](bot, args, message)  # pass command args as str

        # command was not valid 
        else:
            await channel.send("{} typed '{}'. I don't know what that means. Use '{} help' to print the help menu.".format(message.author.name, original, TRIGGER))

@client.event
async def on_ready():
    for guild in client.guilds:
        print('Logged in as {} to {}'.format(client.user.name, guild.name))
    print('------')

    for guild in client.guilds:
        bot = GambleCake(guild)
        bots[guild.id] = GambleCake(guild)
        await bot.online_message()

        

@client.event
async def on_connect():
    pass

@client.event
async def on_disconnect():
    botlog_channel = client.get_channel(695749897554755704)
    logoff_message = "GambleCake going offline at " + datetime.datetime.now().strftime("%H:%M:%S")
    # await botlog_channel.send(logoff_message)


client.run(TOKEN)






