

class Character:
    def __init__(self, name):
        self.name = name
        self.status = {}
        self.stats = {}

        # states of being range from 1-9, 1 bad and 9 being good
        self.status['happiness']     = 5 # muh serotonin
        self.status['fitness']       = 5 # do you even lift
        self.status['health']        = 5 # dying?
        self.status['food']          = 5 # are you eating?
        self.status['reputation']    = 5 # are you a fucking criminal?
        self.status['credit']        = 5 # poor?
        self.status['family']        = 5 # infidel?
        
        # abilities need to be leveled, 1 bad 9 good
        self.stats['strength']     = 3
        self.stats['dexterity']    = 3
        self.stats['constitution'] = 3
        self.stats['intelligence'] = 3
        self.stats['wisdom']       = 3
        self.stats['charisma']     = 3
        self.stats['luck']         = 3

class PlayerCharacter(Character):
    def __init__(self, name):
        super().__init__(name)

class NonPlayerCharacter(Character):
    def __init__(self, name):
        super().__init__(name)
        self.relation_type = None
        self.relation_target = None

        

